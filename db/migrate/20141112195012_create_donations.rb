class CreateDonations < ActiveRecord::Migration
  def change
    create_table :donations do |t|
      t.decimal :amount
      t.timestamp :date

      t.timestamps
    end
  end
end
